import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import re
import os
import glob

directoryo = "./object_file_directory/"
directoryp = "./point_file_directory/"
filelisto = []
filelistp = []

for root, dirs, filenames in os.walk(directoryo):
    for fo in sorted(filenames):
        filelisto.append(fo)

for root, dirs, filenames in os.walk(directoryp):
    for fp in sorted(filenames):
        filelistp.append(fp)

# Create figure and axes
fig,ax = plt.subplots(1)

n=0
# Create a Rectangle patch

for file1 in filelisto:
    file1=open(directoryo+file1,"r")
    for line in file1:
        plt.axis([0,150,-40,40])
        s1 = line[0:].split(':')
        entry_data = s1[0].split(';')
        entry_data[0] = entry_data[0].replace('(','')
        entry_data[0] = entry_data[0].replace(')','')
        entry_data[1] = entry_data[1].replace('(','')
        entry_data[1] = entry_data[1].replace(')','')
        entry_data[3] = entry_data[3].replace('(','')
        entry_data[3] = entry_data[3].replace(')','')
        centerpoint = entry_data[0].split(',')
        centerx = float(centerpoint[0])
        centery = float(centerpoint[1])
        velocity = entry_data[1].split(',')
        velocityx = float(velocity[0])
        velocityy = float(velocity[1])
        boundingbox = entry_data[3].split(',')
        boundingx = float(boundingbox[0])
        boundingy = float(boundingbox[1])
        
        if (boundingx != 0 and boundingy != 0):
            rect = patches.Arc((centerx,centery),boundingx,boundingy)
        arrow = patches.Arrow(centerx,centery,velocityx*0.5,velocityy*0.5,0.5,color='r')
        # Add the patch to the Axes
        ax.add_patch(rect)
        ax.add_patch(arrow)

    file2 = filelistp[n]
    file2 = open(directoryp+file2,'r') 
    for linep in file2:
        p1 = linep[0:].split(':')
        p_data = p1[0].split(' ')
        pointx = float(p_data[0])
        pointy = float(p_data[1])
        circle = patches.Circle((pointx,pointy),0.2)
        ax.add_patch(circle)

    #plt.show()
    plt.tight_layout()
    plt.savefig('./figures/figure'+str(n)+'.png', dpi=100)
    n+=1
    plt.cla()
