import os.path
import datetime
import re


file = open("/input.txt", "r") 

for line in file:
	s1 = line[0:].split(']')
    #using regex so we can split both ; and : from the same string
	entry_data = re.split(';|:',s1[0])
	name = "./data2/ds2_"+ str(datetime.datetime.now())  + ".txt"
    #define n so we know which iteration of the loop we are in to remove empty timestamp
	n=0
	
	if(os.path.isfile(name)):		
		out = open(name, "a")
    #checks if the entry data has empty values or not, using 2 because of timestamps
	elif len(entry_data) == 2:
		continue
	else:
		out = open(name, "w")
		out.write(";")
		out.write(str(len(entry_data) - 2))
		out.write("\nCenter point\tRelative V\tAbsolute V\tBounding Box\tOA\tHSA\tHS")
		out.write("\n")



	for point in entry_data[0:-1]:
        #the first iteration of the loop is always a timestamp, so we skip it
		if n == 0:
			n=+1
			continue
		else:
			n=+1
        #split each element of the object
		pointc = point.split('|')
        part0 = str(pointc[0])
        part1 = str(pointc[1])
        part2 = str(pointc[2])
        part3 = str(pointc[3])
        part4 = str(pointc[4])
        part5 = str(pointc[5])
        part6 = str(pointc[6])
        
        outstr =  part0+"\t"+part1+"\t"+part2+"\t"+part3+"\t"+part4+"\t"+part5+"\t"+part6+"\n"
        out.write(outstr)
	out.close()
file.close()
